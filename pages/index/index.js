var a = getApp();

Page({
    data: {
        motto: "Hello World",
        userInfo: {},
        hasUserInfo: !1,
        canIUse: wx.canIUse("button.open-type.getUserInfo"),
        sbTxt: "李四¹²³⁴⁵⁶", 
        sbTxt1: "王五₁₂₃₄₅₆",
        sbLetterTxt: "张三ᴴᵉˡˡᵒ"
    },
    onShareAppMessage: function(a) {
        return a.from, {
            title: "上下标生成器",
            path: "/pages/index/index"
        };
    },
    bindReward: function(a) {
        wx.previewImage({
          current: [ "http://www.liangmann.com/images/zs.png" ],
          urls: [ "http://www.liangmann.com/images/zs.png" ]
        });
    },
    bindsbNum: function(a) {
        for (var e = a.detail.value, s = [], t = 0; t < e.length; t++) switch (s.push(e.charAt(t)), 
        e.charAt(t)) {
          case "0":
            s[t] = "⁰";
            break;

          case "1":
            s[t] = "¹";
            break;

          case "2":
            s[t] = "²";
            break;

          case "3":
            s[t] = "³";
            break;

          case "4":
            s[t] = "⁴";
            break;

          case "5":
            s[t] = "⁵";
            break;

          case "6":
            s[t] = "⁶";
            break;

          case "7":
            s[t] = "⁷";
            break;

          case "8":
            s[t] = "⁸";
            break;

          case "9":
            s[t] = "⁹";
        }
        this.setData({
            sbNum: a.detail.value,
            sbTxt: s.join("")
        });
    },
    bindsbNum1: function(a) {
        for (var e = a.detail.value, s = [], t = 0; t < e.length; t++) switch (s.push(e.charAt(t)), 
        e.charAt(t)) {
          case "0":
            s[t] = "₀";
            break;

          case "1":
            s[t] = "₁";
            break;

          case "2":
            s[t] = "₂";
            break;

          case "3":
            s[t] = "₃";
            break;

          case "4":
            s[t] = "₄";
            break;

          case "5":
            s[t] = "₅";
            break;

          case "6":
            s[t] = "₆";
            break;

          case "7":
            s[t] = "₇";
            break;

          case "8":
            s[t] = "₈";
            break;

          case "9":
            s[t] = "₉";
        }
        this.setData({
            sbNum1: a.detail.value,
            sbTxt1: s.join("")
        });
    },
    bindsbLetter: function (a) {
      for (var e = a.detail.value, s = [], t = 0; t < e.length; t++) switch (s.push(e.charAt(t)),
        e.charAt(t)) {
          case "a":
            s[t] = "ᵃ";
            break;

          case "b":
            s[t] = "ᵇ";
            break;

          case "c":
            s[t] = "ᶜ";
            break;

          case "d":
            s[t] = "ᵈ";
            break;

          case "e":
            s[t] = "ᵉ";
            break;

          case "f":
            s[t] = "ᶠ";
            break;
          case "g":
            s[t] = "ᵍ";
            break;

          case "h":
            s[t] = "ʰ";
            break;

          case "i":
            s[t] = "ⁱ";
            break;

          case "j":
            s[t] = "ʲ";
            break;

          case "k":
            s[t] = "ᵏ";
            break;
            
          case "l":
            s[t] = "ˡ";
            break;

          case "m":
            s[t] = "ᵐ";
          break;

          case "n":
            s[t] = "ⁿ";
            break;

          case "o":
            s[t] = "ᵒ";
            break;

          case "p":
            s[t] = "ᵖ";
            break;

          case "q":
            s[t] = "q";
            break;

          case "r":
            s[t] = "ʳ";
            break;

          case "s":
            s[t] = "ˢ";
            break;

          case "t":
            s[t] = "ᵗ";
            break;

          case "u":
            s[t] = "ᵘ";
            break;

          case "v":
            s[t] = "ᵛ";
            break;

          case "w":
            s[t] = "ʷ"; 
            break;

          case "x":
            s[t] = "ˣ";
            break;

          case "y":
            s[t] = "ʸ";
            break;

          case "z":
            s[t] = "ᶻ";
          break;

          case "A":
            s[t] = "ᴬ";
            break;

          case "B":
            s[t] = "ᴮ";
            break;

          case "C":
            s[t] = "ᶜ";
            break;

          case "D":
            s[t] = "ᴰ";
            break;

          case "E":
            s[t] = "ᴱ";
            break;

          case "F":
            s[t] = "F";
            break;
          case "G":
            s[t] = "ᴳ";
            break;

          case "H":
            s[t] = "ᴴ";
            break;

          case "I":
            s[t] = "ᴵ";
            break;

          case "J":
            s[t] = "ᴶ";
            break;

          case "K":
            s[t] = "ᴷ";
            break;
            
          case "L":
            s[t] = "ᴸ";
            break;

          case "M":
            s[t] = "ᴹ";
            break;

          case "N":
            s[t] = "ᴺ";
            break;

          case "O":
            s[t] = "ᴼ";
            break;

          case "P":
            s[t] = "ᴾ";
            break;

          case "Q":
            s[t] = "Q";
            break;

          case "R":
            s[t] = "ᴿ";
            break;

          case "S":
            s[t] = "ˢ";
            break;

          case "T":
            s[t] = "ᵀ";
            break;

          case "U":
            s[t] = "ᵁ";
            break;

          case "V":
            s[t] = "ⱽ";
            break;

          case "W":
            s[t] = "ᵂ";
            break;

          case "X":
            s[t] = "ˣ";
            break;

          case "Y":
          s[t] = "ᵞ";
            break;

          case "Z":
            s[t] = "ᶻ";
            break;
          }
      this.setData({
        sbLetter: a.detail.value,
        sbLetterTxt: s.join("")
      });
    },
    copy: function() {
        wx.setClipboardData({
            data: this.data.sbTxt,
            success: function(a) {}
        });
    },
    copy1: function() {
        wx.setClipboardData({
            data: this.data.sbTxt1,
            success: function(a) {}
        });
    },
    copy2: function () {
      wx.setClipboardData({
        data: this.data.sbLetterTxt,
        success: function (a) { }
      });
    },
    onLoad: function() {
        var e = this;
        a.globalData.userInfo ? this.setData({
            userInfo: a.globalData.userInfo,
            hasUserInfo: !0
        }) : this.data.canIUse ? a.userInfoReadyCallback = function(a) {
            e.setData({
                userInfo: a.userInfo,
                hasUserInfo: !0
            });
        } : wx.getUserInfo({
            success: function(s) {
                a.globalData.userInfo = s.userInfo, e.setData({
                    userInfo: s.userInfo,
                    hasUserInfo: !0
                });
            }
        });
    },
    getUserInfo: function(e) {
        console.log(e), a.globalData.userInfo = e.detail.userInfo, this.setData({
            userInfo: e.detail.userInfo,
            hasUserInfo: !0
        });
    }
});